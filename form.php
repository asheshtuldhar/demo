<?php 
	include 'config/connect.php';
	include 'config/formfunction.php';

    if(isset($_POST['submitBtn'])) {
        if(saveData($conn,$_POST)) {
            set_msg('Record saved successfully.');
        }else{ 
            set_msg('Error saving record.');
        }
    }

    if(isset($_GET['id'])) {
    	$id = $_GET['id'];
   		$record = getRecordByID($conn,$id)->fetch_assoc();
    }
?>

<form action="form.php" method="post">
	<?php get_msg(); ?><p>
	<div>
		<label>Firstname</label>
		<input type="text" name="firstname" value="<?php echo isset($record) ? $record['firstname'] : '' ?>">
	</div><br>
	<div>
		<label>Lastname</label>
		<input type="text" name="lastname" value="<?php echo isset($record) ? $record['lastname'] : '' ?>">
	</div>
	<br>
	<button type="submit" name="submitBtn"><?php echo isset($record) ? 'Update' : 'Save' ?></button>
</form>