<?php 
	include 'config/connect.php';
	include 'config/formfunction.php';
    include 'config/assets.php';
    
    $task = getAllRecord($conn);
?>

<a href="form.php" type="button">Create</a><p>
<table border="1">
    <thead>
        <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php if(!empty($task)) : ?>
            <?php while($row = $task->fetch_assoc()) : ?>
                <tr>
                    <td><?php echo $row['firstname'] ?></td>
                    <td><?php echo $row['lastname'] ?></td>
                    <td>
                        <a href="form.php?id=<?php echo $row['id'] ?>">Edit</a>
                        <a href="">Delete</a>
                    </td>
                </tr>
            <?php endwhile ?>
        <?php endif ?>

    </tbody>
</table>